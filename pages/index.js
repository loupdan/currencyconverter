import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import styled from 'styled-components'
import moment from 'moment'
import Nav from '../components/nav'
import CurrencyCalculator from '../components/currencyCalculator'
import HistoricChange from '../components/historicChange'
import {FaMoneyBillAlt} from 'react-icons/fa'

const Global = styled.div`
  font: 16px 'Dosis', sans-serif;
`

const Header = styled.header`
  text-align: center;
  a{
    background: #78BCAC;
    display: inline-block;
    padding: 1em;
    border-radius: 50%;
    margin: 1em;
    svg{
      fill: #fff;
      font-size: 3em;
      filter: drop-shadow(8px 8px 0 #384D48);
    }    
  }
`

const Home = props => {
  return(
  <Global>
    <Head>
      <title>moneyxchange</title>
      <link rel="icon" href="/favicon.ico" />
      <link href="https://fonts.googleapis.com/css?family=Dosis:400,700&display=swap" rel="stylesheet"></link>
    </Head>

    <Header>
      <Link href="/">
        <a>
          <FaMoneyBillAlt />
        </a>
      </Link>
    </Header>

    <Nav />

    <CurrencyCalculator base={props.data.base} rates={props.data.rates}/>

    <HistoricChange rates={props.historicData.rates} />
    
  </Global>
)}

Home.getInitialProps = async function() {
  const baseURL = process.env.BASE_API_URL
  const accessKey = process.env.REACT_APP_API_KEY
  const lastThreeDays = moment().subtract(3, 'days').format('YYYY-MM-DD')

  const res = await fetch(`${baseURL}latest`)
  const data = await res.json()

  const resHistoric = await fetch(`${baseURL}${lastThreeDays}`)
  const historicData  = await resHistoric.json()
  //console.log(`Show data fetched. Count: ${data.length}`);

  return {
    data,
    historicData
  }
}
export default Home
