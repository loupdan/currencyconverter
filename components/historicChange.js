import React from 'react'
import styled from 'styled-components'
import { Container, Row, Col } from 'react-grid-system'

const SecctionTitle = styled.h3`
    color:#187C65;
    margin: 2em 0;
    font-size: 1.8em;
`

const CustomRow =styled.div`
    justify-content: center;
    &>div{
        background: #9CD8CA;
        margin: .5em 0;
    }
    .currency{
        font-size: 1.5em;
        color: #384D48;
    }
    .value{
        font-size: 1.2em;
        color: #187C65;
    }
`

const HistoricChange = props =>{
    return(
        <Container>
            <Row>
                <Col sm={12}>
                    <SecctionTitle>HISTORIC PRICE</SecctionTitle>
                </Col>
            </Row>
            <Row component={CustomRow}>
                {Object.keys(props.rates).map((item, index)=>(
                        <Col  offset={{sm: 1}} sm={5} key={index}> 
                            <p className="currency">
                                <strong>Currency: </strong>  
                                {Object.keys(props.rates)[index]}
                            </p>
                            <p className="value">
                                <strong>Value: </strong>
                                {new Intl.NumberFormat("en-US",{style: "currency", currency: Object.keys(props.rates)[index]})
                                .format(props.rates[item])}
                            </p>
                        </Col>
                    )
                )}
            </Row>
        </Container>
    )
}

export default HistoricChange