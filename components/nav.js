import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

const links = [
  { href: 'https://google.com/', label: 'Google' },
  { href: 'https://facebook.com/', label: 'Facebook' },
  { href: 'https://twitter.com/', label: 'Twitter' },
]

const Ul = styled.ul`
  display: flex;
  justify-content: center;
  margin: 1em 0;
  padding: 0;
  li{
    list-style-type: none;
    margin: .3em;
  }
  a{
    background: #9CD8CA;
    color: #384D48;
    text-decoration: none;
    padding: .5em;
    border-radius: .3em;
    &:hover{
      color: #9CD8CA;
      background: #384D48;
    }
  }
`

const Nav = () => (
  <nav>
    <Ul>
      {links.map((item, key) => (
        <li key={key}>
          <a href={item.href}>{item.label}</a>
        </li>
      ))}
    </Ul>

  </nav>
)

export default Nav
