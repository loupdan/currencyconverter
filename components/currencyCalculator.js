import React, { useState } from 'react'
import fetch from'isomorphic-unfetch'
import styled from 'styled-components'
import { Container, Row, Col } from 'react-grid-system'

const SectionContainer = styled.div`
    background: #384D48;
    padding: 2em 0;
    h2{
        color: #fff
    }
    form{
        text-align: center;
        input{
            border-radius: 5px;
            border: solid 1px #9CD8CA;
            width: 100%;
            line-height: 2em;
            margin: 1em 0;
            font-size: 1.5em;
            &[type="submit"]{
                background: #187C65;
                color: #fff;
                font-size: 1.2em;
            }
            &[disabled]{
                background:#9CD8CA;
            }
        }
    }
`

const CurrencyCalculator = props => {

    const [convertionValue , setConvertionValue] = useState ('EUR') 

    const handleSubmit = event =>{
        event.preventDefault()
        const originCurrency = 'USD'
        const destinyCurrency = 'EUR'
        const originValue = event.target.originCurrency.value
        const currentValue = originValue / props.rates.USD
        const convertion = new Intl.NumberFormat("de-DE",{style: "currency", currency: destinyCurrency}).format(currentValue)
        setConvertionValue(convertion)
    }
    
    return(
        <SectionContainer>
            <Container>
                <Row justify="center">
                    <Col sm={12}>
                        <h2>Currency Converter Calculator</h2>
                    </Col>
                </Row>
                <form noValidate onSubmit={handleSubmit}>
                    <Row>
                    
                        <Col md={6} sm={12}>
                            <input type="number" name="originCurrency" placeholder="USD"/>
                        </Col>
                        <Col md={6} sm={12}>
                            <input type="text" disabled name="destinyCurrency" placeholder="EUR" value={convertionValue}/>
                        </Col>
                    </Row>
                    <Row justify="center">
                        <Col md={6} sm={12}>
                            <input type="submit" value="Calculate"/>
                        </Col>
                    </Row>
                </form>
            </Container>
        </SectionContainer>
    )
}

export default CurrencyCalculator